//
//  ViewController.swift
//  try
//
//  Created by Ananya M on 17/01/20.
//  Copyright © 2020 Ananya M. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var addItemView: UIView!
    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    var effect: UIVisualEffect!
    @IBAction func clossButtonTapped(_ sender: Any) {
        self.addItemView.removeFromSuperview()
        self.visualEffectView.effect = nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        effect = visualEffectView.effect
        visualEffectView.effect = nil
        addItemView.layer.cornerRadius = 5
    }

    func animateIn() {
        self.view.addSubview(addItemView)
        
        addItemView.center = self.view.center
        addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        addItemView.alpha = 0
        
        
            UIView.animate(withDuration: 0.4) {
                self.visualEffectView.effect = self.effect
                self.addItemView.alpha = 1
                self.addItemView.transform = CGAffineTransform.identity
        }
    }
    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
         
            self.addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.addItemView.alpha = 0
            self.visualEffectView.effect = nil
        }) { (succes: Bool) in
            self.addItemView.removeFromSuperview()
        }
    
    }
    @IBAction func addItem(_ sender: Any) {
        animateIn()
    }
    
    @IBAction func dismissPopUp(_ sender: Any) {
        animateOut()
    }
}

